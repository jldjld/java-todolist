package com.example.demo.presentation.controller;

import java.util.List;

import com.example.demo.dto.TodoItem;
import com.example.demo.dto.api.TodoRequest;
import com.example.demo.services.TodoListService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import jakarta.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/api/todo")
@SecurityRequirement(name = "todojwt")
public class TodoApiController {
    @Autowired
    private TodoListService todoListService;
    private static final Logger logger = LoggerFactory.getLogger(TodoApiController.class);

    /**
     *
     * @return
     */
    @GetMapping("")
    public ResponseEntity<Iterable<TodoItem>> apiTodolist() {
            return todoListService.getTodoList();
    }

    /**
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public TodoItem apiTodo(@PathVariable final Long id) throws ResponseStatusException {
        return todoListService.apiGetTodo(id);
    }

    @GetMapping("/{content}/search")
    public ResponseEntity<List<TodoItem>> searchTodo(@PathVariable final String content) throws ResponseStatusException {
        return todoListService.searchTodoItem(content);
    }

    @PostMapping("/add")
    public ResponseEntity<TodoItem> addTodo(@Valid @RequestBody final TodoRequest request) throws ResponseStatusException {
        if( request.getContent().isEmpty() ) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Missing request body.");
        }

        return todoListService.createTodoItem(request.getContent());
    }

    @PostMapping(path = "/{id}/complete")
    public ResponseEntity<Void> completeTodo(@PathVariable final Long id) throws ResponseStatusException {
        return todoListService.completeTodoItem(id);
    }

    @PostMapping(path = "/update")
    public ResponseEntity<Void> updateTodo(@Valid @RequestBody final TodoRequest request) throws ResponseStatusException {
        return todoListService.updateTodoItem(request);
    }
}