package com.example.demo.presentation.controller;

import com.example.demo.config.jwt.JwtAuthToken;
import com.example.demo.config.jwt.JwtProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/jwt")
public class JwtController {

    private final JwtProvider jwtProvider;

    private static final Logger logger = LoggerFactory.getLogger(JwtController.class);

    @Autowired
    private AuthenticationManager authenticationManager;

    public JwtController(JwtProvider jwtProvider) {
        this.jwtProvider = jwtProvider;
    }

    @GetMapping("/authenticate")
    public ResponseEntity<JwtAuthToken> getToken(@RequestHeader("X-Shared-Secret") String sharedKey) {
        if( sharedKey.isEmpty() ) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }

        String generatedToken = this.jwtProvider.generateToken( sharedKey );
        if( generatedToken.isEmpty() ) {
            return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).build();
        }

        JwtAuthToken authenticationToken = jwtProvider.validateToken(generatedToken);

        SecurityContextHolder.getContext().setAuthentication(authenticationToken);
        return new ResponseEntity<>(
                authenticationToken,
                HttpStatus.OK
        );
    }
}