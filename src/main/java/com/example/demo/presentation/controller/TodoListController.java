package com.example.demo.presentation.controller;

import com.example.demo.dto.TodoItem;
import com.example.demo.dto.TodoItemRepository;
import com.example.demo.services.TodoListService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Consumer;

@Controller
public class TodoListController {

    @Autowired
    private TodoListService todoListService;
    private static final Logger logger = LoggerFactory.getLogger(TodoListController.class);

    /**
     * Display todolist
     */
//    @GetMapping("/todos")
//    public String getTodolist( Model model) {
//        if (!this.getTodos().isEmpty()) {
//            model.addAttribute("todolist", this.getTodos());
//            model.addAttribute("completeTodo", (Consumer<TodoItem>) item -> {
//                // completeTodo(item);
//                logger.info("todo " + item.getId() + " completed");
//            });
//        }
//
//        return "todolist";
//    }

    /**
     * Post new todoItem
     *
     * @param todoItem
     * @param model
     * @return
     */
    @PostMapping("/todos/add")
    public String postTodoItem(@ModelAttribute TodoItem todoItem, Model model) {
        String content = todoItem.getContent();
        if (!content.isEmpty()) {
            TodoItem newTodo = this.todoListService.createTodoForController(content);
            todoListService.todoList.add(newTodo);
            model.addAttribute("todolist", todoListService.todoList);
            // Redirect to todos to avoid form reload
            return "redirect:/todos";
        }

        return "todolist";
    }

    /**
     * Creates item if not already exists
     *
     * @param content
     * @return
     * @throws Error
     */
    public ResponseEntity<TodoItem> createTodoItem(String content) {
        return todoListService.createTodoItem(content);
    }


    /*public void completeTodo( TodoItem todo ) {
        todoListService.completeTodoItem(todo);
    }*/
    /**
     * Get todolist from service
     *
     * @return
     */
//    public ArrayList<TodoItem> getTodos() {
//        return todoListService.getTodoList();
//    }
}
