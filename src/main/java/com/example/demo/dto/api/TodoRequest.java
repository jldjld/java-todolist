package com.example.demo.dto.api;

import io.swagger.v3.oas.annotations.Hidden;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

public class TodoRequest {

    @GeneratedValue(strategy= GenerationType.AUTO)
    @Hidden
    private Long id;

    @NotBlank(message = "Content value cannot be null.")

    private String content;

    @NotNull
    @Hidden
    private boolean completed;

    public Long getId() { return id; }

    public void setId( final Long id ) { this.id = id; }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

}
