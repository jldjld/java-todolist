package com.example.demo.dto;

import io.swagger.v3.oas.annotations.Hidden;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.validation.constraints.NotNull;

@Entity
public class TodoItem {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    private String content;

    @NotNull

    private Boolean completed = false;

    public Long getId() { return id; }

    @Override
    public String toString() {
        return "TodoItem{id='" + id + "', content=" + content + "', completed=" + completed + "}";
    }
    public String getContent() { return content; }

    public Boolean getCompleted() { return completed; }

    public void setId( final Long id ) { this.id = id; }

    public void setContent( final String content ) { this.content = content; }

    public void setCompleted( final Boolean completed ) { this.completed = completed; }
}

