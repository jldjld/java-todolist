package com.example.demo.dto;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TodoItemRepository extends CrudRepository<TodoItem, Long> {
    TodoItem findById(long id);
    List<TodoItem> findByContentContainingIgnoreCase(final String content);
}