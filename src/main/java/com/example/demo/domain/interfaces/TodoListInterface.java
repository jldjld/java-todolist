package com.example.demo.domain.interfaces;

import com.example.demo.dto.TodoItem;
import com.example.demo.dto.TodoItemRepository;
import com.example.demo.dto.api.TodoRequest;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public interface TodoListInterface {
        ResponseEntity<Iterable<TodoItem>> getTodoList();
        ResponseEntity<TodoItem> createTodoItem(String content);

        TodoItem createTodoForController(String item);

        TodoItem apiGetTodo(final Long id);
        ResponseEntity<Void> completeTodoItem(final Long id);

        ResponseEntity<Void> updateTodoItem(final TodoRequest todo);

        ResponseEntity<List<TodoItem>> searchTodoItem(String content);
}
