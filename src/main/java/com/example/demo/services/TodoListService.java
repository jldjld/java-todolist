package com.example.demo.services;

import com.example.demo.domain.interfaces.TodoListInterface;
import com.example.demo.dto.TodoItem;
import com.example.demo.dto.TodoItemRepository;
import com.example.demo.dto.api.TodoRequest;
import com.example.demo.presentation.controller.TodoListController;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.config.ConfigDataResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class TodoListService implements TodoListInterface {

    @Autowired
    private TodoItemRepository todoItemRepository;
    public final ArrayList<TodoItem> todoList = new ArrayList<TodoItem>();
    private static final Logger logger = LoggerFactory.getLogger(TodoListController.class);

    public TodoListService(TodoItemRepository todoItemRepository) {
        this.todoItemRepository = todoItemRepository;
    }

    @Override
    public ResponseEntity<Iterable<TodoItem>> getTodoList() {
        Iterable<TodoItem> todos = this.todoItemRepository.findAll();
        if (!todos.iterator().hasNext()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }

        return ResponseEntity.ok(todos);
    }

    @Override
    public TodoItem apiGetTodo(Long id) throws ResponseStatusException {
        return this.todoItemRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Todo Not found."));
    }

    @Override
    public ResponseEntity<TodoItem> createTodoItem(String content) throws ResponseStatusException {
        if(content.isEmpty()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }

        List<TodoItem> existingTodo = this.todoItemRepository.findByContentContainingIgnoreCase(content);
        if(!existingTodo.isEmpty()) {
            return ResponseEntity.status(HttpStatus.CONFLICT).build();
        }

        String cleanContent = content.toLowerCase();
        TodoItem todoItem   = new TodoItem();
        todoItem.setContent(cleanContent);

        // Save to database
        TodoItem newTodo = this.todoItemRepository.save(todoItem);
        if(newTodo.getId() == null) {
            return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).build();
        }

        return ResponseEntity.ok(newTodo);
    }

    @Override
    public TodoItem createTodoForController(String content) {
        String cleanContent = content.toLowerCase();
        TodoItem todoItem   = new TodoItem();
        todoItem.setContent(cleanContent);

        // Save to database
        return this.todoItemRepository.save(todoItem);
    }

    @Override
    public ResponseEntity<Void> completeTodoItem(final Long id) throws ResponseStatusException {
        Optional<TodoItem> todo = this.todoItemRepository.findById(id);
        if(!todo.isPresent()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }

        TodoItem todoItem = todo.get();
        todoItem.setCompleted(!todoItem.getCompleted());

        TodoItem completedTodo = this.todoItemRepository.save(todoItem);
        if(completedTodo.getId() == null) {
            return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).build();
        }

        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    public ResponseEntity<Void> updateTodoItem( final TodoRequest todo ) throws ResponseStatusException {
        String todoContent = todo.getContent();
        Long todoId        = todo.getId();
        if( todoId == null || todoContent.isEmpty() ) {
            return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).build();
        }

        Optional<TodoItem> todoItem = this.todoItemRepository.findById(todoId);
        if(!todoItem.isPresent()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        TodoItem updatedtodoItem = todoItem.get();
        updatedtodoItem.setContent(todoContent);

        TodoItem savedTodo = todoItemRepository.save(updatedtodoItem);
        if(savedTodo.getId() == null) {
            return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).build();
        }

        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @Override
    public ResponseEntity<List<TodoItem>> searchTodoItem( String content ) throws ResponseStatusException {
        String formattedContent = content.replaceAll("[^a-zA-Z0-9]", " ").toLowerCase();
        List<TodoItem> todos = this.todoItemRepository.findByContentContainingIgnoreCase(formattedContent);
        if(todos.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }

        return ResponseEntity.ok(todos);
    }
}
