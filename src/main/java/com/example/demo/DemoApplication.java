package com.example.demo;
import com.example.demo.dto.TodoItem;
import com.example.demo.dto.TodoItemRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class DemoApplication {
	private static final Logger log = LoggerFactory.getLogger(DemoApplication.class);
	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	@Bean
	public CommandLineRunner demo(TodoItemRepository repository) {
		return (args) -> {
			// save a few todos
			TodoItem itemTest1 = new TodoItem();
			itemTest1.setContent("sel");
			repository.save(itemTest1);

			TodoItem itemTest2 = new TodoItem();
			itemTest2.setContent("poivre");
			repository.save(itemTest2);

			// fetch all todos
			log.info("Todos found with findAll():");
			log.info("-------------------------------");
			for (TodoItem todo : repository.findAll()) {
				log.info(todo.getContent());
			}
			log.info("");
		};
	}
}