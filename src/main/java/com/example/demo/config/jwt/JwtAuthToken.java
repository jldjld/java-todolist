package com.example.demo.config.jwt;

import com.fasterxml.jackson.databind.util.JSONPObject;
import io.swagger.v3.core.util.Json;
import jakarta.annotation.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Date;

public class JwtAuthToken extends AbstractAuthenticationToken {
    private transient String principal;

    private final Long expiration;

    private static final Logger logger = LoggerFactory.getLogger(JwtAuthToken.class);


    public JwtAuthToken(Collection<? extends GrantedAuthority> authorities, String token, UserDetails details, @Nullable Long expiration ) {
        super(authorities);
        this.principal = token;
        this.expiration = expiration;
        super.setDetails(details);
        setAuthenticated(true);
    }

    @Override
    public Object getCredentials() {
        return "";
    }

    @Override
    public String getPrincipal() { return this.principal; }

    public Long getExpiration() { return this.expiration; }
}
