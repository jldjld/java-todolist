package com.example.demo.config.jwt;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;


@Component
public class JwtFilter extends OncePerRequestFilter {
    @Autowired
    private JwtProvider jwtProvider;
    /**
     * Exclude auth path from being filtered
     * @param request
     * @return
     * @throws ServletException
     */
    @Override
    protected boolean shouldNotFilter(HttpServletRequest request) throws ServletException {
        String path = request.getServletPath();

        if ( path.startsWith("/swagger-ui/") ||
                path.startsWith("/v3/api-docs") ||
                path.startsWith("/jwt/authenticate") ) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Check all routes except from the one present in shouldNotFilter
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doFilterInternal(final HttpServletRequest request, final HttpServletResponse response, final FilterChain chain) throws ServletException, IOException {
        String token = request.getHeader("Authorization");
        if (token == null) {
            throw new ServletException("Authorization missing.");
        }

        if ( token.toLowerCase().startsWith("bearer ")) {
            token = token.substring(7);
        }

        Authentication authenticationToken = this.jwtProvider.validateToken(token);
        if( authenticationToken instanceof BadCredentialsException ) {
            throw new ServletException("Token is not valid.");
        }

        SecurityContextHolder.getContext().setAuthentication(authenticationToken);
        chain.doFilter(request, response);
    }
}
