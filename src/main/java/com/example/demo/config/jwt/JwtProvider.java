package com.example.demo.config.jwt;

import com.example.demo.presentation.controller.model.TodoUserDetails;
import io.jsonwebtoken.*;
import io.jsonwebtoken.security.Keys;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.*;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import java.time.Duration;
import java.time.Instant;
import java.util.Collections;
import java.util.Date;

@Component
public class JwtProvider implements AuthenticationProvider {

    //@Value("${sharedSecretKey}")
    public static String todoSecretKey = "todojwtsecretfortodolisttoken1234";
    private static final Logger logger = LoggerFactory.getLogger(JwtProvider.class);
    private final Instant now = Instant.now();
    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws ResponseStatusException {
        return authentication;
    }

    /**
     * @param sharedKey
     * @return
     */
    public String generateToken(String sharedKey) {
        Instant expiration = now.plus(Duration.ofHours(1));
        return Jwts.builder()
                .setExpiration(Date.from(expiration))
                .signWith(
                    Keys.hmacShaKeyFor(sharedKey.getBytes())
                )
                .compact();
    }

    /**
     *
     * @param token
     * @return
     * @throws BadCredentialsException
     */
    public JwtAuthToken validateToken(String token) throws BadCredentialsException {
        try {
            Claims claims = Jwts.parserBuilder()
                    .setSigningKey(Keys.hmacShaKeyFor(todoSecretKey.getBytes()))
                    .build()
                    .parseClaimsJws(token)
                    .getBody();

            if( Date.from(now).getTime() > claims.getExpiration().getTime()) {
                throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
            }

            return new JwtAuthToken(Collections.emptyList(), token, new TodoUserDetails(), claims.getExpiration().getTime());
        } catch (ExpiredJwtException e) {
            throw new BadCredentialsException("Authentication token expired.");
        }
    }
}
