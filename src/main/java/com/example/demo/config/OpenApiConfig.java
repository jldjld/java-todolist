package com.example.demo.config;


import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;


@Configuration
@SecurityScheme(
        type = SecuritySchemeType.HTTP,
        name = "todojwt",
        scheme = "bearer",
        bearerFormat = "JWT" )
public class OpenApiConfig {
        @Bean
        public OpenAPI usersMicroserviceOpenAPI() {
            return new OpenAPI()
                    .info(new Info().title("TodoList API")
                            .description("Add, complete and display todos.")
                            .version("1.0"));
        }
    }

