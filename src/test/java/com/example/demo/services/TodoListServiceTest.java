package com.example.demo.services;

import com.example.demo.dto.TodoItem;
import com.example.demo.dto.TodoItemRepository;
import com.example.demo.dto.api.TodoRequest;
import com.example.demo.presentation.controller.TodoListController;
import org.junit.jupiter.api.*;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.*;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TodoListServiceTest {

    private static final Logger log = LoggerFactory.getLogger(TodoListController.class);

    private TodoItemRepository todoItemRepository;

    @Autowired
    private ApplicationContext applicationContext;
    @Captor
    ArgumentCaptor<TodoItem> todoItemCaptor;

    // mock creation
    TodoItemRepository repository   = mock(TodoItemRepository.class);
    TodoListService todoListService = new TodoListService(repository);
    TodoRequest itemTest1Request = new TodoRequest();

    TodoItem itemTest1 = new TodoItem();
    TodoItem itemTest2 = new TodoItem();
    TodoItem itemTest3 = new TodoItem();

    List<TodoItem> todos = new ArrayList<>();

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        // Reset the mock object
        Mockito.reset(repository);
        todoItemRepository = applicationContext.getBean(TodoItemRepository.class);

        // save a few todos
        itemTest1.setContent("sel");
        todoItemRepository.save(itemTest1);

        itemTest2.setContent("poivre");
        todoItemRepository.save(itemTest2);

        itemTest3.setContent("sel");
        itemTest3.setId(8L);
        todoItemRepository.save(itemTest3);

        // Create a list to test search function
        todos.add(itemTest1);
        todos.add(itemTest2);
        todos.add(itemTest3);

        // Create request entity
        itemTest1Request.setContent("sel");
        itemTest1Request.setId(1L);
    }
    @Test
    void getTodoList() {
        when(repository.findAll()).thenReturn(todos);

        ResponseEntity<Iterable<TodoItem>> getTodos = todoListService.getTodoList();
        Mockito.verify(repository, Mockito.times(1)).findAll();

        Assertions.assertEquals(getTodos, getTodos);
    }

    @Test
    void apiGetTodo() {
        when(repository.findById(itemTest2.getId())).thenReturn(Optional.of(itemTest2));

        TodoItem getTodo = todoListService.apiGetTodo( itemTest2.getId() );
        Mockito.verify(repository, Mockito.times(1)).findById(itemTest2.getId());

        Assertions.assertEquals(getTodo, itemTest2);
    }

    /**
     * Testing with non existing ID
     */
    @Test
    void apiGetTodoKo() {
        when(repository.findById(10L)).thenReturn(null);

        Assertions.assertThrows(ResponseStatusException.class, () -> {
            todoListService.apiGetTodo(10L);
        });
    }

    @Test
    void createTodoItem() {
        ArgumentCaptor<TodoItem> potatoCaptor = ArgumentCaptor.forClass(TodoItem.class);

        TodoItem savedPotato = new TodoItem();
        savedPotato.setContent("patate");
        savedPotato.setId(5L);

        when(repository.save(potatoCaptor.capture())).thenReturn(savedPotato);

        ResponseEntity<TodoItem> testNewTodo = todoListService.createTodoItem("patate");

        Mockito.verify(repository, Mockito.times(1)).save(potatoCaptor.getValue());
        Assertions.assertNotNull(testNewTodo.getBody());
        Assertions.assertEquals(savedPotato.getId(), testNewTodo.getBody().getId());
    }

    /**
     * Test with empty todoItem name
     */
    @Test
    void createEmptyTodoItemKo() {
        ResponseEntity<TodoItem> testNewTodo = todoListService.createTodoItem("");
        Assertions.assertEquals(HttpStatus.BAD_REQUEST, testNewTodo.getStatusCode());
    }

    /**
     * Test with already existing todoItem
     */
    @Test
    void createExistingTodoItemKo() {
        when(repository.findByContentContainingIgnoreCase("sel")).thenReturn(todos);
        ResponseEntity<TodoItem> testNewTodo = todoListService.createTodoItem("sel");

        Mockito.verify(repository, Mockito.times(1)).findByContentContainingIgnoreCase("sel");
        Assertions.assertEquals(HttpStatus.CONFLICT, testNewTodo.getStatusCode());
    }

    @Test
    void SaveTodoItemKo() {
        ArgumentCaptor<TodoItem> potatoCaptor = ArgumentCaptor.forClass(TodoItem.class);

        TodoItem savedPotato = new TodoItem();

        when(repository.save(potatoCaptor.capture())).thenReturn(savedPotato);

        ResponseEntity<TodoItem> testNewTodo = todoListService.createTodoItem("thyme");
        Assertions.assertNull(testNewTodo.getBody());
        Assertions.assertEquals(HttpStatus.UNPROCESSABLE_ENTITY, testNewTodo.getStatusCode());
    }

    @Test
    void completeTodoItem() {
        when(repository.findById(itemTest1.getId())).thenReturn(Optional.of(itemTest1));
        when(repository.save(itemTest1)).thenReturn(itemTest1);

        ResponseEntity<Void> completedTodo = todoListService.completeTodoItem( itemTest1.getId() );

        Mockito.verify(repository, Mockito.times(1)).findById(itemTest1.getId());
        Mockito.verify(repository, Mockito.times(1)).save(itemTest1);

        Assertions.assertTrue(completedTodo.getStatusCode().isSameCodeAs(HttpStatus.NO_CONTENT));
    }

    @Test
    void searchTodoItem() {
        // Returns list of todos if have same content
        when(repository.findByContentContainingIgnoreCase("sel")).thenReturn(todos);

        ResponseEntity<List<TodoItem>> completedTodo = todoListService.searchTodoItem( "sel" );

        Mockito.verify(repository, Mockito.times(1)).findByContentContainingIgnoreCase("sel");

        Assertions.assertEquals(HttpStatus.OK, completedTodo.getStatusCode());
        Assertions.assertEquals(todos, completedTodo.getBody());
    }

    /**
     * Search todoItem not existing in todoList
     */
    @Test
    void searchTodoItemKo() {
        //Create an empty List
        List<TodoItem> emptyList = new ArrayList<>();
        when(repository.findByContentContainingIgnoreCase("curry")).thenReturn(emptyList);

        ResponseEntity<List<TodoItem>> completedTodo = todoListService.searchTodoItem( "curry" );
        Mockito.verify(repository, Mockito.times(1)).findByContentContainingIgnoreCase("curry");

        Assertions.assertEquals(HttpStatus.NOT_FOUND, completedTodo.getStatusCode());
    }


    void updateTodoItem() {
        when(repository.findById(itemTest1Request.getId())).thenReturn(Optional.of(itemTest1));
        when(repository.save(itemTest1)).thenReturn(itemTest1);

        ResponseEntity<Void> updatedTodo = todoListService.updateTodoItem( itemTest1Request );

        Mockito.verify(repository, Mockito.times(1)).findById(itemTest1Request.getId());
        Mockito.verify(repository, Mockito.times(1)).save(itemTest1);

        Assertions.assertTrue(updatedTodo.getStatusCode().isSameCodeAs(HttpStatus.NO_CONTENT));
    }

    void updateTodoItemKo() {
        TodoRequest savedPotatoRequest = new TodoRequest();
        savedPotatoRequest.setContent("");
        savedPotatoRequest.setId(5L);

        ResponseEntity<Void> updatedTodo = todoListService.updateTodoItem( savedPotatoRequest );
        Assertions.assertTrue(updatedTodo.getStatusCode().isSameCodeAs(HttpStatus.UNPROCESSABLE_ENTITY));
    }
}